﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Sample.Web.Models
{
    public class LoginModel:BaseModel
    {
        //[Required(ErrorMessageResourceType = typeof(Sample.Web.App_GlobalResources.Sample), ErrorMessageResourceName = "UsernameRequired")]
        [Required(ErrorMessage = "User Name requierd")]
        //[Display(ResourceType = typeof(Sample.Web.App_GlobalResources.Sample), Name = "Username")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Sample.Web.App_GlobalResources.Sample), ErrorMessageResourceName = "PasswordRequired")]
        //[Display(ResourceType = typeof(Sample.Web.App_GlobalResources.Sample), Name = "Password")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        //[StringLength(16, MinimumLength = 8, ErrorMessageResourceType = typeof(Sample.Web.App_GlobalResources.Sample), ErrorMessageResourceName = "PasswordStringLenght")]
        public string Password { get; set; }
    }
}