﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Sample.Web.Models
{
    public class UserModel : BaseModel
    {
        [Required(ErrorMessage="Enter your First Name")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please select your gender")]
        [Display(Name = "Gender")]
        public char Gender { get; set; }

        [Required(ErrorMessage = "Enter your Date of birth ")]
        [Display(Name = "Date of birth")]
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }

        [Required(ErrorMessage = "Enter your address ")]
        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "Enter your city")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Enter your state ")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "Enter your  pin code ")]
        [Display(Name = "Pin code")]
        public int? Pincode { get; set; }

        [Required(ErrorMessage = "Enter your mobile number ")]
        [Display(Name = "Mobile Number")]
        public decimal? MobileNumber { get; set; }

        [Required(ErrorMessage = "Enter your Email Id ")]
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "User Name is required ")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Enter your password  ")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Reenter the password")]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string ConformPassword { get; set; }

    }
}