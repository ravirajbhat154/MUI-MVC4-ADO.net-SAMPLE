﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sample.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override ViewResult View(string ViewName, string masterName, object model)
        {
            return PrepareView(ViewName, masterName, model);
        }

        private ViewResult PrepareView(string ViewName, string masterName, object model)
        {
            ViewResult renderview = null;

            if (System.IO.File.Exists(Server.MapPath(VirtualPathUtility.ToAbsolute("~/Views/Shared" + "/" + ViewName + ".cshtml"))))
            {
                renderview = base.View("~/Views/Shared/" + ViewName + ".cshtml", masterName, model);
            }
            else
            {
                renderview = base.View("~/Views/Sample/" + ViewName + ".cshtml", masterName, model);
            }

            if (renderview != null)
                return renderview;

            return base.View(ViewName, masterName, model);
        }

        protected new ViewResult View(string ViewName)
        {
            return PrepareView(ViewName, null, null);
        }

        protected new ViewResult View(string ViewName, object model)
        {
            return PrepareView(ViewName, null, model);
        }

    }
}
