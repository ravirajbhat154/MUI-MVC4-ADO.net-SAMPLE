﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sample.Web.Models;
using BizUserService = Sample.Biz.Contract.IUserService;
using BizUserServiceImpl = Sample.Biz.Impl.UserServiceImpl;
namespace Sample.Web.Controllers
{
    public class UserController : BaseController
    {
        private BizUserService userService = null;
        public UserController()
        {
            userService = new BizUserServiceImpl();
        }
        [HttpGet]
        public ActionResult SignUp()
        {
            return View("SignUp", new UserModel());
        }

        [HttpPost]
        public ActionResult SignUp(UserModel userModel)
        {
            
            if (!ModelState.IsValid)
            {
                return View("SignUp", userModel);
            }
            Sample.Biz.Data.User userObj = new Biz.Data.User() {
                FirstName = userModel.FirstName,
                MiddleName = userModel.MiddleName,
                LastName = userModel.LastName,
                DateOfBirth= (DateTime)userModel.DateOfBirth,
                Gender = userModel.Gender.ToString(),
                Address1 = userModel.Address1,
                Address2 = userModel.Address2,
                City=userModel.City,
                State=userModel.State,
                ZipCode = Convert.ToString(userModel.Pincode),
                PhoneNumber= Convert.ToString(userModel.MobileNumber),
                Email= userModel.EmailId,
                UserName= userModel.UserName,
                Password= userModel.Password,
            };
            var userid=userService.AddEdit(userObj);
            return View("Login", new LoginModel());

          
        }

    }
}
