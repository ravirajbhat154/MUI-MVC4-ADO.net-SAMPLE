﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sample.Core.Contract;
using Sample.Core.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Sample.Core.Impl
{
    public class UserServiceImpl : IUserService
    {
        public int Save(User user)
        {
            user.DTCreate = DateTime.Now;
            return AddEditUser(user, EventType.ADD);

        }

        public int Update(User user)
        {
            user.DTLastMod = DateTime.Now;
            return AddEditUser(user, EventType.EDIT);
        }

        public User Get(int id)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(connectionString);
            var command = new SqlCommand("[spGetUserById]", cnn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            cnn.Open();
            command.ExecuteNonQuery();
            cnn.Close();
            SqlDataReader read = command.ExecuteReader();
            DataTable objDataTable = new DataTable();
            objDataTable.Load(read);
            return convertToUser(objDataTable);
        }

        public User Get(string userName, string password)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(connectionString);
            var command = new SqlCommand("[spAuthenticateUser]", cnn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@UserName", SqlDbType.NVarChar, 20).Value = userName;
            command.Parameters.Add("@Password", SqlDbType.NVarChar, 20).Value = password;
            cnn.Open();
            command.ExecuteNonQuery();
            cnn.Close();
            SqlDataReader read = command.ExecuteReader();
            DataTable objDataTable = new DataTable();
            objDataTable.Load(read);
            return convertToUser(objDataTable);
        }

        private int AddEditUser(User user, EventType eventType)
        {
            int result = 0;
            var connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(connectionString);
            var command = new SqlCommand("[spAddEditUser]", cnn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@EventType", SqlDbType.NVarChar, 20).Value = eventType.ToString();
            command.Parameters.Add("@Id", SqlDbType.Int).Value = user.Id;
            command.Parameters.Add("@UserName", SqlDbType.NVarChar, 20).Value = user.UserName;
            command.Parameters.Add("@Password", SqlDbType.NVarChar, 20).Value = user.Password;
            command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 20).Value = user.FirstName;
            command.Parameters.Add("@MiddleName", SqlDbType.NVarChar, 20).Value = user.MiddleName;
            command.Parameters.Add("@LastName", SqlDbType.NVarChar, 20).Value = user.LastName;
            command.Parameters.Add("@IsEnabled", SqlDbType.Bit).Value = user.IsEnabled;
            command.Parameters.Add("@ManagerId", SqlDbType.Int).Value = user.ManagerId;
            command.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar, 20).Value = user.PhoneNumber;
            command.Parameters.Add("@Email", SqlDbType.NVarChar, 50).Value = user.Email;
            command.Parameters.AddWithValue("@DOB", user.DateOfBirth);
            command.Parameters.Add("@Gender", SqlDbType.NVarChar, 6).Value = user.Gender;
            command.Parameters.Add("@Address1", SqlDbType.VarChar, 255).Value = (object)user.Address1 ?? DBNull.Value;
            command.Parameters.Add("@Address2", SqlDbType.VarChar, 255).Value =(object) user.Address2 ?? DBNull.Value;
            command.Parameters.Add("@City", SqlDbType.VarChar, 100).Value = (object)user.City ?? DBNull.Value;
            command.Parameters.Add("@State", SqlDbType.VarChar, 20).Value = (object)user.State ?? DBNull.Value;
            command.Parameters.Add("@ZipCode", SqlDbType.NVarChar, 20).Value = (object)user.ZipCode ?? DBNull.Value;
            command.Parameters.AddWithValue("@DTCreate", user.DTCreate);
            command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = (object)user.CreatedBy ?? DBNull.Value;
            command.Parameters.AddWithValue("@DTLastMod", (object)user.DTLastMod ?? DBNull.Value);
            command.Parameters.Add("@ModBy", SqlDbType.Int).Value = (object)user.ModifiedBy ?? DBNull.Value;
            command.Parameters.Add("@userId", SqlDbType.Int);
            command.Parameters["@userId"].Direction = ParameterDirection.Output;
            cnn.Open();
            command.ExecuteNonQuery();
            cnn.Close();
            result = Convert.ToInt32(command.Parameters["@userId"].Value.ToString());
            return result;
        }

        private User convertToUser(DataTable objDataTable)
        {
            List<User> cList = Helper.DataTableToList<User>(objDataTable);
            if (cList != null && cList.Count>0)
            {
                return cList[0];
            }

            return null;
        }

        //public bool GetUserById(int id)
        //{
        //    Console.WriteLine("********************************************************************************");
        //    var connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        //    SqlConnection cnn = new SqlConnection(connectionString);
        //    try
        //    {
        //        cnn.Open();
        //        var command = new SqlCommand("spGetUserById", cnn);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@Id", SqlDbType.Int).Value = id;

        //        command.ExecuteNonQuery();
        //        SqlDataReader read = command.ExecuteReader();
        //        if (read.Read())
        //        {
        //            Console.WriteLine("User Id\t\t:"+read["userID"]);
        //            Console.WriteLine("User Name\t:" + read["userName"]);
        //            //Console.WriteLine("Password\t:"+read["password"]);
        //            Console.WriteLine("First Name\t:" + read["firstName"]);
        //            Console.WriteLine("Middle Name\t:" + read["middleName"]);
        //            Console.WriteLine("Last Name\t:" + read["lastName"]);
        //            Console.WriteLine("DOB\t\t:"+read["dateOfBirth"]);
        //            Console.WriteLine("Gender\t\t:" + read["Gender"]);
        //            Console.WriteLine("Address1\t:" + read["Address1"]);
        //            Console.WriteLine("Address2\t:" + read["Address2"]);
        //            Console.WriteLine("City\t\t:" + read["City"]);
        //            Console.WriteLine("State\t\t:" + read["State"]);
        //            Console.WriteLine("Pin code\t:" + read["Pincode"]);
        //            Console.WriteLine("Mobile Number \t:" + read["mobileNumber"]);
        //            Console.WriteLine("Email Id\t:" + read["emailId"]);

        //        }
        //        else
        //        {
        //            throw new Exception("No User id present in db......please reenter ");
        //        }
        //        cnn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    return true; 

        //}
        //public bool AuthenticationUser(string name, string password)
        //{
        //    Console.WriteLine("********************************************************************************");
        //    var connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        //    SqlConnection cnn = new SqlConnection(connectionString);
        //    try
        //    {
        //        cnn.Open();
        //        var command = new SqlCommand("spAuthenticationUserName", cnn);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@uname", SqlDbType.VarChar,50).Value = name;
        //        command.Parameters.Add("@password", SqlDbType.VarChar,20).Value = password;

        //        command.ExecuteNonQuery();
        //        SqlDataReader read = command.ExecuteReader();
        //        if (read.Read())
        //        {
        //            Console.WriteLine("User Id\t\t:" + read["userID"]);
        //            //`Console.WriteLine("User Name\t:" + read["userName"]);
        //            //Console.WriteLine("Password\t:"+read["password"]);
        //            Console.WriteLine("First Name\t:" + read["firstName"]);
        //            Console.WriteLine("Middle Name\t:" + read["middleName"]);
        //            Console.WriteLine("Last Name\t:" + read["lastName"]);
        //            Console.WriteLine("DOB\t\t:" + read["dateOfBirth"]);
        //            Console.WriteLine("Gender\t\t:" + read["Gender"]);
        //            Console.WriteLine("Address1\t:" + read["Address1"]);
        //            Console.WriteLine("Address2\t:" + read["Address2"]);
        //            Console.WriteLine("City\t\t:" + read["City"]);
        //            Console.WriteLine("State\t\t:" + read["State"]);
        //            Console.WriteLine("Pin code\t:" + read["Pincode"]);
        //            Console.WriteLine("Mobile Number \t:" + read["mobileNumber"]);
        //            Console.WriteLine("Email Id\t:" + read["emailId"]);

        //        }
        //        else
        //        {
        //            throw new Exception("Either username or password is incorrect");
        //        }
        //        cnn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    return true;
        //}
        //public int Save(User user)
        //{
        //    Console.WriteLine("********************************************************************************");
        //    int result=0;
        //    var connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        //    SqlConnection cnn = new SqlConnection(connectionString);
        //    try
        //    {

        //        var command = new SqlCommand("spCreateUser", cnn);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@firstName", SqlDbType.VarChar,50).Value =user.FirstName ;
        //        command.Parameters.Add("@middleName", SqlDbType.VarChar,50).Value = user.MiddleName;
        //        command.Parameters.Add("@lastName", SqlDbType.VarChar,50).Value=user.LastName;
        //        command.Parameters.AddWithValue("@dateOfBirth",user.DateOfBirth);
        //        command.Parameters.Add("@Gender", SqlDbType.Char).Value = user.Gender;
        //        command.Parameters.Add("@Address1", SqlDbType.VarChar,255).Value = user.Address1;
        //        command.Parameters.Add("@Address2", SqlDbType.VarChar, 255).Value = user.Address2;
        //        command.Parameters.Add("@City", SqlDbType.VarChar, 255).Value = user.City;
        //        command.Parameters.Add("@State", SqlDbType.VarChar, 255).Value = user.State;
        //        command.Parameters.Add("@Pincode", SqlDbType.Decimal,6).Value = user.Pincode;
        //        command.Parameters.Add("@mobileNumber", SqlDbType.Decimal,10).Value = user.MobileNumber;
        //        command.Parameters.Add("@emailId", SqlDbType.VarChar, 255).Value = user.emailId;
        //        command.Parameters.Add("@userName", SqlDbType.VarChar, 50).Value = user.userName;
        //        command.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = user.password;
        //        command.Parameters.Add("@userId", SqlDbType.Int);
        //        command.Parameters["@userId"].Direction = ParameterDirection.Output;


        //        cnn.Open();
        //        command.ExecuteNonQuery();
        //        cnn.Close();
        //        result = Convert.ToInt32(command.Parameters["@userId"].Value.ToString());

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    return result;

        //}
    }
}
