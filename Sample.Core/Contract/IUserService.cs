﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sample.Core.Data;

namespace Sample.Core.Contract
{
    public interface IUserService
    {
        int Save(User user);

        int Update(User user);

        User Get(int id);

        User Get(string userName, string password);
    }
}

