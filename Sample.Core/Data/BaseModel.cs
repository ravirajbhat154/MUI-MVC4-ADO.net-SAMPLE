﻿using System;

namespace Sample.Core.Data
{
    public class BaseModel
    {
        public int Id { get; set; }
        public DateTime? DTCreate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? DTLastMod { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
