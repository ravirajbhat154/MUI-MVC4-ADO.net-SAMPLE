--exec [spAddEditUser] 'ADD',0,'RAVI','7458','RAJ','','',1,0,'','',null,'','','','','','',null,0,null,null,0
--select * from tUser
CREATE PROCEDURE [dbo].[spAddEditUser]
	@EventType [nvarchar](20),
	@Id [int],
	@UserName [nvarchar](20),
	@Password [varchar](20),
	@FirstName [nvarchar](20),
	@MiddleName [nvarchar](20),
	@LastName [nvarchar](20),
	@IsEnabled [bit],
	@ManagerId [int],
	@PhoneNumber [nvarchar](20),
	@Email [nvarchar](50),
	@DOB [datetime],
	@Gender [nvarchar](6),
	@Address1 [nvarchar](255),
	@Address2 [nvarchar](255),
	@City [nvarchar](100),
	@State [nvarchar](20),
	@ZipCode [nvarchar](20),
	@DTCreate [datetime],
	@CreatedBy [int],
	@DTLastMod [datetime],
	@ModBy [int],
	@UserId int output
AS
	IF @EventType = 'ADD'
	BEGIN
		INSERT INTO [dbo].[tUser]
			([UserName],[Password],[FirstName],[MiddleName],[LastName],[IsEnabled],
			[ManagerId],[PhoneNumber],[Email],[DOB],[Gender],[Address1],
			[Address2],[City],[State],[ZipCode],[DTCreate],[CreatedBy])
		VALUES
		   (@UserName,@Password,@FirstName,@MiddleName,@LastName,@IsEnabled,
		   @ManagerId,@PhoneNumber,@Email,@DOB,@Gender,@Address1,
		   @Address2,@City,@State,@ZipCode,@DTCreate,@CreatedBy)
		
		SELECT @UserId=Max([Id]) FROM [dbo].[tUser]
	END
	ELSE IF @EventType = 'EDIT'
	BEGIN
		UPDATE [dbo].[tUser]
	    SET [UserName] = @UserName,
		  [Password] = @Password,
		  [FirstName] = @FirstName,
		  [MiddleName]=@MiddleName,
		  [LastName] = @LastName,
		  [IsEnabled] = @IsEnabled,
		  [ManagerId] = @ManagerId,
		  [PhoneNumber] = @PhoneNumber,
		  [Email] = @Email,
		  [DOB] = @DOB,
		  [Gender] = @Gender,
		  [Address1] = @Address1,
		  [Address2] = @Address2,
		  [City] = @City,
		  [State] = @State,
		  [ZipCode] = @ZipCode,
		  [DTLastMod] = @DTLastMod,
		  [ModBy] = @ModBy
		WHERE [Id]= @Id
		
		SELECT @UserId=[Id] FROM [dbo].[tUser] WHERE [Id]= @Id
	END
GO


