﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using AutoMapper;
using Sample.Biz.Contract;
using CoreUserService= Sample.Core.Contract.IUserService;
using CoreUserServiceImpl = Sample.Core.Impl.UserServiceImpl;
using Sample.Biz.Data;
namespace Sample.Biz.Impl
{
    public class UserServiceImpl : IUserService
    {
        private CoreUserService userService = null;

        public UserServiceImpl()
        {
            this.userService = new CoreUserServiceImpl();
            Mapper.CreateMap<User, Sample.Core.Data.User>();
        }

        public bool GetUserById(int id)
        {
            Sample.Core.Data.User user=userService.Get(id);
            return user != null ? true : false;
        }

        public bool AuthenticationUser(string name, string password)
        {
            Sample.Core.Data.User user = userService.Get(name, password);
            return user != null ? true : false;
        }

        public int AddEdit(User user)
        {

            Sample.Core.Data.User coreUser = Mapper.Map<Sample.Core.Data.User>(user);

            return userService.Save(coreUser);
        }
    }
}