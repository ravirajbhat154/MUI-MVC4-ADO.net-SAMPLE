﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sample.Biz.Data;

namespace Sample.Biz.Contract
{
  
        public interface IUserService
        {
            int AddEdit(User user);
            bool GetUserById(int id);
            bool AuthenticationUser(string name, string password);
        }
}

